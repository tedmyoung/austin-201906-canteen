package com.visa.ncg.canteen;

public class InvalidAmountException extends RuntimeException {
  public InvalidAmountException(String message) {
    super(message);
  }

  public InvalidAmountException() {
  }
}
