package com.visa.ncg.canteen;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountJpaRepository extends JpaRepository<AccountDto, Long> {
}
