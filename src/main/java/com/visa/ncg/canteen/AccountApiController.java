package com.visa.ncg.canteen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountApiController {

  private final AccountRepository accountRepository;

  @Autowired
  public AccountApiController(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @GetMapping("/api/accounts/{id}")
  public AccountResponse accountInfo(@PathVariable("id") String accountId) {
    Long id = Long.valueOf(accountId);

    Account account = accountRepository.findOne(id);

    AccountResponse accountResponse = new AccountResponse(account);

    return accountResponse;
  }

  @PostMapping("/api/accounts")
  public AccountResponse createAccount(@RequestBody AccountCreateRequest request) {
    Account account = new Account();
    account.changeNameTo(request.getAccountName());
    account.deposit(request.getInitialBalance());

    Account savedAccount = accountRepository.save(account);

    return new AccountResponse(savedAccount);
  }
}
