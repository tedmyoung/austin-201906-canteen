package com.visa.ncg.canteen;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class FakeAccountRepository implements AccountRepository {

  private final Map<Long, Account> accountMap = new HashMap<>();
  private AtomicLong sequence = new AtomicLong();

  public Account findOne(Long id) {
    return accountMap.get(id);
  }

  public Account save(Account entity) {
    assignIdIfNeeded(entity);
    accountMap.put(entity.getId(), entity);
    return entity;
  }

  private void assignIdIfNeeded(Account entity) {
    if (entity.getId() == null) {
      entity.setId(sequence.getAndIncrement());
    }
  }

  public List<Account> findAll() {
    return new ArrayList<>(accountMap.values());
  }

}