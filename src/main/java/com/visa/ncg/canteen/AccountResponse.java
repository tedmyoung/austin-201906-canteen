package com.visa.ncg.canteen;

public class AccountResponse {
  private long id;
  private int balance;
  private String name;

  public AccountResponse() {
  }

  public AccountResponse(Account account) {
    setId(account.getId());
    setBalance(account.balance());
    setName(account.name());
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }
}
