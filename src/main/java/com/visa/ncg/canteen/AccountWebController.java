package com.visa.ncg.canteen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AccountWebController {

  private AccountRepository accountRepository;

  @Autowired
  public AccountWebController(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @GetMapping("/account/{id}")
  public String accountView(@PathVariable("id") String id, Model model) {
    Long accountId = Long.parseLong(id);
    Account account = accountRepository.findOne(accountId);

    if (account == null) {
      throw new NoSuchAccountException();
    }

    AccountResponse accountResponse = new AccountResponse(account);

    model.addAttribute("account", accountResponse);
    return "account-view";
  }

  @GetMapping("/account")
  public String allAccountsView(Model model) {
    List<Account> accounts = accountRepository.findAll();

    List<AccountResponse> responses = convertToResponses(accounts);

    model.addAttribute("accounts", responses);
    return "all-accounts";
  }

  private List<AccountResponse> convertToResponses(List<Account> accounts) {
    return accounts.stream()
                   .map(AccountResponse::new)
                   .collect(Collectors.toList());
  }

  @ExceptionHandler(NoSuchAccountException.class)
  public ModelAndView handleNotFound(Model model) {
    model.addAttribute("message", "Couldn't find account.");
    ModelAndView modelAndView = new ModelAndView("404");
    modelAndView.setStatus(HttpStatus.NOT_FOUND);
    return modelAndView;
  }

  @GetMapping("/create-account")
  public String createAccountForm(Model model) {
    CreateForm createForm = new CreateForm();
    createForm.setAccountName("");
    createForm.setInitialDeposit(100);
    model.addAttribute("createForm", createForm);
    return "create-account";
  }

  @PostMapping("/create-account")
  public String createAccount(@ModelAttribute("createForm") CreateForm createForm) {
    Account account = new Account();
    account.changeNameTo(createForm.getAccountName());
    account.deposit(createForm.getInitialDeposit());

    Account savedAccount = accountRepository.save(account);

    return "redirect:/account/" + savedAccount.getId(); // this is a URL not a view
  }
}
