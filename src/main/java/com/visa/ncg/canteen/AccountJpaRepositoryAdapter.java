package com.visa.ncg.canteen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Primary
@Repository
public class AccountJpaRepositoryAdapter implements AccountRepository {

  private static final Logger logger = LoggerFactory.getLogger(AccountJpaRepositoryAdapter.class);

  private AccountJpaRepository accountJpaRepository;

  @Autowired
  public AccountJpaRepositoryAdapter(AccountJpaRepository accountJpaRepository) {
    this.accountJpaRepository = accountJpaRepository;
  }

  @Override
  public Account findOne(Long id) {
    logger.info("Looking for account with ID {}", id);

    AccountDto dto = accountJpaRepository.findById(id).get();
    return dto.asAccount();
  }

  @Override
  public Account save(Account entity) {
    logger.info("Saving account with name '{}'", entity.name());

    AccountDto dto = AccountDto.from(entity);
    AccountDto savedDto = accountJpaRepository.save(dto);
    return savedDto.asAccount();
  }

  @Override
  public List<Account> findAll() {
    List<Account> accounts = accountJpaRepository
        .findAll()
        .stream()
        .map(AccountDto::asAccount)
        .collect(Collectors.toList());


    logger.info("Found {} accounts in the database", accounts.size());

    return accounts;
  }
}
