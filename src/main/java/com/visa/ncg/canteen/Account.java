package com.visa.ncg.canteen;

public class Account {
  private int balance;
  private String name;
  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public int balance() {
    return balance;
  }

  public void deposit(int amount) {
    validateAmount(amount);
    balance = balance + amount;
  }

  public void withdraw(int amount) {
    validateAmount(amount);
    validateSufficientBalance(amount);
    balance -= amount;
  }

  private void validateSufficientBalance(int amount) {
    if (amount > balance) {
      throw new InsufficientBalanceException();
    }
  }

  private void validateAmount(int amount) {
    if (amount <= 0) {
      throw new InvalidAmountException();
    }
  }

  public String name() {
    return name;
  }

  public void changeNameTo(String newName) {
    name = newName;
  }
}
