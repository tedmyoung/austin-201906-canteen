package com.visa.ncg.canteen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AccountDataLoader implements ApplicationRunner {
  private AccountRepository accountRepository;

  @Autowired
  public AccountDataLoader(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    // create a couple of accounts with initial balance and save them to the repository
    Account account1 = new Account();
    account1.deposit(10);
    account1.changeNameTo("Fun stuff");
    accountRepository.save(account1); // gets ID of 0
    Account account2 = new Account();
    account2.deposit(20);
    account2.changeNameTo("Necessities");
    accountRepository.save(account2); // gets ID of 1
  }
}
