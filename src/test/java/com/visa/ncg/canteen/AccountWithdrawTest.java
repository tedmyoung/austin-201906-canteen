package com.visa.ncg.canteen;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountWithdrawTest {
  @Test
  public void withdraw3FromAccountHaving7ResultsIn4Balance() throws Exception {
    // Given
    Account account = new Account();
    account.deposit(7);

    account.withdraw(3);

    assertThat(account.balance())
        .isEqualTo(4);
  }

  @Test
  public void withdraw3Then4FromAccountWithBalanceOf12ResultsIn5() throws Exception {
    Account account = new Account();
    account.deposit(12);

    account.withdraw(3);
    account.withdraw(4);

    assertThat(account.balance())
        .isEqualTo(5);
  }

  @Test
  public void withdraw0ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> account.withdraw(0))
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdrawNegative1ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> account.withdraw(-1))
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void balanceOf2Withdraw6ThrowsInsufficientBalanceException() throws Exception {
    Account account = new Account();
    account.deposit(2);


    assertThatThrownBy(() -> account.withdraw(6))
        .isInstanceOf(InsufficientBalanceException.class);
  }

  @Test
  public void canWithdrawBalanceDownToZero() throws Exception {
    Account account = new Account();
    account.deposit(7);

    account.withdraw(7);

    assertThat(account.balance())
        .isZero();
  }
}
