package com.visa.ncg.canteen;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountApiControllerTest {

  @Test
  public void testGetMapping() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    account.deposit(73);
    account.changeNameTo("Test");
    account.setId(123L);
    accountRepository.save(account);

    AccountApiController controller = new AccountApiController(accountRepository);

    AccountResponse accountResponse = controller.accountInfo("123");

    assertThat(accountResponse.getBalance())
        .isEqualTo(73);
    assertThat(accountResponse.getName())
        .isEqualTo("Test");

  }

}