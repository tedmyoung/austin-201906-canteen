package com.visa.ncg.canteen;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositorySaveTest {

  @Test
  public void savedAccountCanBeFoundByFindOne() throws Exception {
    AccountRepository repo = new FakeAccountRepository();
    Account account = new Account();
    account.setId(9L);

    repo.save(account);

    assertThat(repo.findOne(9L).getId())
        .isEqualTo(9L);
  }

  @Test
  public void newlySavedAccountIsAssignedAnId() throws Exception {
    AccountRepository repo = new FakeAccountRepository();
    Account account = new Account();

    Account savedAccount = repo.save(account);

    assertThat(savedAccount.getId())
        .isNotNull();
  }

  @Test
  public void newlySavedAccountsHaveUniqueIds() {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account1 = new Account();
    account1 = accountRepository.save(account1);
    Account account2 = new Account();
    account2 = accountRepository.save(account2);

    assertThat(account1.getId())
        .isNotEqualTo(account2.getId());
  }

  @Test
  public void saveAccountWithIdHasChangedAttributesWhenFound() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    account.deposit(15);
    account.setId(12L);
    accountRepository.save(account);

    // When we retrieve the account, manipulate its balance, and then save (update) it
    Account found = accountRepository.findOne(12L);
    found.withdraw(7);
    accountRepository.save(found);

    // Then it should have the new balance when we find it in the repository
    Account account12 = accountRepository.findOne(12L);
    assertThat(account12.balance())
        .isEqualTo(8);
  }

}
