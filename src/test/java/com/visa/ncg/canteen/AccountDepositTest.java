package com.visa.ncg.canteen;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountDepositTest {

  @Test
  public void newAccountBalanceIsZero() throws Exception {
    Account account = new Account();

    assertThat(account.balance())
        .isZero();
  }

  @Test
  public void deposit10ResultsInBalanceOf10() throws Exception {
    // Given a new account
    Account account = new Account();

    // when I deposit 10
    account.deposit(10);

    // Then I expect balance to be 10
    assertThat(account.balance())
        .isEqualTo(10);
  }

  @Test
  public void deposit7ToAccountWith12ResultsInBalanceOf19() throws Exception {
    // Given
    Account account = new Account();
    account.deposit(12);

    // When
    account.deposit(7);

    // Then
    assertThat(account.balance())
        .isEqualTo(19);
  }

  @Test
  public void depositZeroThrowsInvalidAmountException() throws Exception {
    Account account = new Account();


    assertThatThrownBy(() -> {
      account.deposit(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void depositNegative1ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();


    assertThatThrownBy(() -> account.deposit(-1))
        .isInstanceOf(InvalidAmountException.class);
  }
}
